# journals/admin.py
from django.contrib import admin

from .models import Journal, Review


class ReviewInline(admin.TabularInline):
    model = Review


class JournalAdmin(admin.ModelAdmin):
    inlines = [
        ReviewInline,
    ]
    list_display = ['title', 'number', 'total_number', 'year', 'start_month', 'end_month', 'redactor', 'price']


admin.site.register(Journal, JournalAdmin)
