# books/models.py
from datetime import date
import uuid
from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse


class Journal(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
    CHOICES = (
        ('Янв', 'Январь',),
        ('Фев', 'Февраль',),
        ('Мар', 'Март',),
        ('Апр', 'Апрель',),
        ('Май', 'Май',),
        ('Июн', 'Июнь',),
        ('Июл', 'Июль',),
        ('Авг', 'Август',),
        ('Сен', 'Сентябрь',),
        ('Окт', 'Октябрь',),
        ('Ноя', 'Ноябрь',),
        ('Дек', 'Декабрь',),
    )
    title = models.CharField(max_length=128)
    redactor = models.CharField(max_length=256)
    number = models.SmallIntegerField(blank=True, null=True)
    total_number = models.IntegerField(blank=True, null=True)
    start_month = models.CharField(max_length=8, choices=CHOICES, blank=True)
    end_month = models.CharField(max_length=8, choices=CHOICES, blank=True)
    year = models.DateField(default=date.today)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    cover = models.ImageField(upload_to='cover/', blank=True)

    def __str__(self):
        return f'{self.title} №{self.number}-{self.year}'

    def get_absolute_url(self):
        return reverse('journal_detail', kwargs={'pk': str(self.pk)})


class Review(models.Model):
    journal = models.ForeignKey(
        Journal,
        on_delete=models.CASCADE,
        related_name='reviews',
    )
    review = models.CharField(max_length=256)
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f'{self.review}'

