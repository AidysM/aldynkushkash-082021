# journals/tests.py
from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.urls import reverse

from .models import Journal, Review


class JournalTests(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='reviewuser',
            email='reviewuser@email.com',
            password='testpass123',
        )
        self.journal = Journal.objects.create(
            title='Harry Potter',
            redactor='JK Rowling',
            price='25.00',
        )
        self.review = Review.objects.create(
            journal=self.journal,
            author=self.user,
            review='An excellent review',
        )
    def test_journal_listing(self):
        self.assertEqual(f'{self.journal.title}', 'Harry Potter')
        self.assertEqual(f'{self.journal.redactor}', 'JK Rowling')
        self.assertEqual(f'{self.journal.price}', '25.00')
    def test_journal_list_view(self):
        response = self.client.get(reverse('journal_list'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Harry Potter')
        self.assertTemplateUsed(response, 'journals/journal_list.html')
    def test_journal_detail_view(self):
        response = self.client.get(self.journal.get_absolute_url())
        no_response = self.client.get('/journals/12345/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'Harry Potter')
        self.assertContains(response, 'An excellent review')
        self.assertTemplateUsed(response, 'journals/journal_detail.html')
