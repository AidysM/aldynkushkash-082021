# books/urls.py
from django.urls import path
from .views import JournalListView, JournalDetailView


urlpatterns = [
    path('', JournalListView.as_view(), name='journal_list'),
    path('<uuid:pk>/', JournalDetailView.as_view(), name='journal_detail'),
]
