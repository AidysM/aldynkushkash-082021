# journals/views.py
from django.views.generic import ListView, DetailView
from .models import Journal


class JournalListView(ListView):
    model = Journal
    context_object_name = 'journal_list'
    template_name = 'journals/journal_list.html'


class JournalDetailView(DetailView):
    model = Journal
    context_object_name = 'journal'
    template_name = 'journals/journal_detail.html'
